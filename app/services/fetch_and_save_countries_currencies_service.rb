require 'httparty'
require_relative '../models/country'

class FetchAndSaveCountriesCurrenciesService
  include HTTParty
  base_uri 'https://graphql.country'

  def self.fetch_countries
    headers = {
      'Content-Type' => 'application/json'
    }

    body = {
      query: '{
        countries {
          edges {
            node {
              name
              capital
              region
              population
              demonym
              borders
              nativeName
              alpha3Code
              languages{
                edges{
                  node{
                    name
                  }
                }
              }
              flag
              currencies {
                edges {
                  node {
                    name
                    code
                    symbol
                  }
                }
              }
            }
          }
        }
      }'
    }

    response = self.post(
      '/graphql',
      body: body.to_json,
      headers: headers
    )

    response.parsed_response['data']['countries']['edges'].map { |edge| edge['node'] } if response.success?
  end

  def self.save_countries_currencies
    fetch_countries.each do |country_data|
      country = Country.create(
        name: country_data['name'],
        capital: country_data['capital'],
        region: country_data['region'],
        population: country_data['population'],
        demonym: country_data['demonym'],
        borders: country_data['borders'],
        nativeName: country_data['nativeName'],
        alpha3Code: country_data['alpha3Code'],
        languages: country_data['languages']['edges'],
        flag: country_data['flag']
      )

      country_data['currencies']['edges'].each do |currency_data|
        if currency_data['node']['name'].present?
          currency = Currency.find_or_create_by(                            # prevents duplicates
            name: currency_data['node']['name'],
            code: currency_data['node']['code'],
            symbol: currency_data['node']['symbol']
          )

          country.currencies << currency
        end
      end
    end
  end
end