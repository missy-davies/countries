# README

### Features

- View list of all countries in the world
- Create/read/update/delete countries and their data

#### TODO

- [ ] Improve styling of country cards and create/edit input form
- [ ] Format borders, languages, flag data -> parse input from form
- [ ] Add currency info
- [ ] Quiz questions

### Technologies Used

- Ruby on Rails for the backend api
- Vue.js + Vite for the frontend app

### Setup

1. Clone this repository, ensure you have ruby, bundler, npm, and mysql installed.
2. `cd` into directory and run `bundle install` then `cd` into `client` and run `npm install`
3. Seed database with country and currency data by running `bin/rails db:seed`
   - Note: This uses data country from this graphql endpoint: https://graphql.country/. You can play with and read the data in GraphiQl here: https://graphql.country/graphql.
4. Start vite and rails servers with `npm run dev` and `rails s`
