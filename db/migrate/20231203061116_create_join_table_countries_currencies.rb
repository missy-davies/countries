class CreateJoinTableCountriesCurrencies < ActiveRecord::Migration[6.1]
  def change
    create_join_table :countries, :currencies do |t|
      t.index [:country_id, :currency_id], unique: true
    end
  end
end
