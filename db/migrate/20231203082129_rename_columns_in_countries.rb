class RenameColumnsInCountries < ActiveRecord::Migration[6.1]
  def change
    rename_column :countries, :nativeName, :native_name
    rename_column :countries, :alpha3Code, :alpha3_code
  end
end
