class CreateCountries < ActiveRecord::Migration[6.1]
  def change
    create_table :countries do |t|
      t.string :name, null: false
      t.string :capital
      t.string :region
      t.integer :population
      t.string :demonym
      t.text :borders, default: nil
      t.string :nativeName
      t.string :alpha3Code
      t.text :languages, default: nil
      t.string :flag

      t.timestamps
    end
  end
end
